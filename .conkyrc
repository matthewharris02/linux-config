--[[
######################################################################
#  __    __   _    _
# |  \  /  | | |  | |   Copyright (c) 2022 Matthew Harris
# |*| \/ |*| |*|__|*|
# |*|    |*| |* __ *|   https:/www.gitlab.com/matthewharris02
# |*|    |*| |*|  |*|
# |_|    |_| |_|  |_|   Conky Configuration
#
######################################################################
]]
conky.config = {
    -- Placement and size
    alignment = 'middle_right',
    minimum_height = 200,
    minimum_width = 280,
    maximum_width = 280,

    -- Grahics
    background = true,
    border_width = 1,
    stippled_borders = 0,
    draw_borders = false,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,

    gap_x = 10,
    gap_y = 10,
    border_inner_margin = 5,
    border_outer_margin = 5,

    -- Colours
    default_color = 'white',
    default_outline_color = 'white',
    default_shade_color = 'white',
    color0 = '2E3440',           -- nord0  | dark1
    color1 = '4c566a',           -- nord3  | dark4
    color2 = 'd8dee9',           -- nord4  | light1
    color3 = 'eceff4',           -- nord6  | light3
    color4 = '8fbcbb',           -- nord7  | blue1
    color5 = '5e81ac',           -- nord10 | blue4
    color6 = 'bf616a',           -- nord11 | red
    color7 = 'd08770',           -- nord12 | orange
    color8 = 'ebcb8b',           -- nord13 | yellow
    color9 = 'a3be8c',           -- nord14 | green

    -- Window Management
    own_window = true,
    own_window_class = 'Conky',
    own_window_argb_value = 150,     -- Real transparency - composite manager required
    own_window_argb_visual = true,
    own_window_transparent = false,
    own_window_type = 'override',

    -- Other
    show_graph_scale = false,
    show_graph_range = false,

    cpu_avg_samples = 2,
    diskio_avg_samples = 10,
    double_buffer = true,           -- Use the Xdbe extension? (eliminate flicker) [From Arcolinux config]
    no_buffers = true,
    use_spacer = 'none',
    out_to_console = false,

    -- Textual
    update_interval = 1.0,
    font = 'Noto Mono:size=12:regular',
    net_avg_samples = 2,
    uppercase = false,
    extra_newline = false,
    out_to_ncurses = false,
    use_xft = true,
    out_to_stderr = false,
    out_to_x = true,
    format_human_readable = true,
    short_units = true,                  -- M, G instead of MiB, GiB
}

conky.text = [[
${color9}${alignc} System Info: ${nodename}${color}
$hr
# Following adapted from Arcolinux qtile conky config
# Author: Erik Dubois

${color2}${goto 100}Used${alignr}Size${color}
${color2}Root${goto 100}${color3}${fs_used /} (${fs_used_perc /}%)${alignr}${fs_size /}${color}
${color2}Storage${goto 100}${color3}${fs_used /storage} (${fs_used_perc /storage}%)${alignr}${fs_size /storage}${color}

${color2}${goto 100}Mem${alignr}Max${color}
${color2}RAM${goto 100}${color2}${mem}${alignr}${memmax}${color}
${color2}Swap${goto 100}${color2}${swap}${alignr}${swapmax}${color}

${color2}CPU:${goto 100}Used${alignr}GHz${color}
${color2}Avg${goto 100}${color2}${if_match ${cpu cpu0}<50}  ${cpu cpu0}\
${else}${if_match ${cpu cpu0}<=100}${color6} ${cpu cpu0}\
${else}${cpu cpu0}${endif}${endif}%${alignr}${freq_g}${color}

${hr}
${color2}${execi 6000 lsb_release -d | grep 'Descr'|awk {'print $2 " " $3" " $4" " $5'}}${alignr}${execi 6000 lsb_release -a | grep 'Release'|awk {'print $2""$3""$4""$5'}}${color}
# END
]]
