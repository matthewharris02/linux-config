"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" --------------- Personal Mappings (inc. plugins) ----------------------------
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" --- Set leader key to: space ---
    let mapleader = " "

" ---------- Plugin Shortcuts (p) ----------
" --- Goyo (g)
    nnoremap <leader>pg :Goyo \| set linebreak<CR>


" --- Vifm (f - filer)
    nnoremap <Leader>fv :Vifm<CR>
    nnoremap <Leader>f\ :VsplitVifm<CR>
    nnoremap <Leader>f- :SplitVifm<CR>
    "nnoremap <Leader>dv :DiffVifm<CR>
    nnoremap <Leader>tf :TabVifm<CR>

" --- NERDTree (n)
    nnoremap <C-n> :NERDTreeToggle<CR>
    " Refresh NERDTree
    nnoremap <Leader>pn :NERDTree<CR>

" --- vim-tmux-navigator
    nnoremap <leader>pt :TmuxNavigatePrevious<CR>

" Note: UltiSnips triggers in plugconf file

" coc.nvim --- (<leader>c)       (adapted from coc README)
    " Rename symbol
    nmap <leader>cr <Plug>(coc-rename)
    " Apply codeAction to selected region
    xmap <leader>ca <Plug>(coc-codeaction-selected)
    " Remap keys for applying codeAction to current buffer
    nmap <leader>cb <Plug>(coc-codeaction)
    " Apply AutoFix to problem on current line
    nmap <leader>cx <Plug>(coc-fix-current)

    " Navigate diagnostics
    nmap <silent> [g <Plug>(coc-diagnostic-prev)
    nmap <silent> ]g <Plug>(coc-diagnostic-next)

    " GoTo code navigation.
    nmap <silent> gd <Plug>(coc-definition)
    nmap <silent> gy <Plug>(coc-type-definition)
    nmap <silent> gi <Plug>(coc-implementation)
    nmap <silent> gr <Plug>(coc-references)

    " Use K to show documentation in preview window.

    " Formatting selected code.
    xmap <leader>cf  <Plug>(coc-format-selected)
    nmap <leader>cf  <Plug>(coc-format-selected)

    " Map function and class text objects
    " NOTE: Requires 'textDocument.documentSymbol' support from the language server.
    xmap if <Plug>(coc-funcobj-i)
    omap if <Plug>(coc-funcobj-i)
    xmap af <Plug>(coc-funcobj-a)
    omap af <Plug>(coc-funcobj-a)
    xmap ic <Plug>(coc-classobj-i)
    omap ic <Plug>(coc-classobj-i)
    xmap ac <Plug>(coc-classobj-a)
    omap ac <Plug>(coc-classobj-a)
    
    nnoremap <silent><leader>cd :CocDiagnostics<CR>
    " Mappings for CoCList
    " Show all diagnostics.
    nnoremap <silent><nowait> <space>cld :<C-u>CocList diagnostics<cr>
    " Manage extensions.
    nnoremap <silent><nowait> <space>cle :<C-u>CocList extensions<cr>
    " Show commands.
    nnoremap <silent><nowait> <space>clc :<C-u>CocList commands<cr>
    " Find symbol of current document.
    nnoremap <silent><nowait> <space>clo :<C-u>CocList outline<cr>
    " Search workspace symbols.
    nnoremap <silent><nowait> <space>cls :<C-u>CocList -I symbols<cr>
    " Do default action for next item.
    nnoremap <silent><nowait> <space>cj :<C-u>CocNext<CR>
    " Do default action for previous item.
    nnoremap <silent><nowait> <space>ck :<C-u>CocPrev<CR>
    " Resume latest coc list.
    nnoremap <silent><nowait> <space>cp :<C-u>CocListResume<CR>
" ---------- Other Shortcuts ---------
    " Easy escape; toggle insert; save on leave insert
     inoremap <silent><C-Space> <Esc>:w<CR>
     nnoremap <C-Space> a
     cnoremap <C-Space> <C-c>
" Escape insert in terminal with esc
    tnoremap <Esc> <C-\><C-n>
    " Open terminal with <leader>
    nnoremap <c-x> :call OpenTerminal()<CR>
" Save on ESC
    inoremap <silent><Esc> <Esc>:w<CR>
" --- Note Tabs (n)

" --- Compile (m - make)

" --- Preview ()

" --- Tabs
    nnoremap <leader>tq :tabclose<CR>
    nnoremap <leader>tn :tabnew<CR>

" --- Windows/panes
    " New vertical split (as on | key without shift)
    nnoremap <leader>\ <c-W>v
    " New horizontal split
    nnoremap <leader>- <c-W>s
    " Remap split to save key presses; integrate with tmux
    nnoremap <c-j> <c-w>j
    nnoremap <c-k> <c-w>k
    nnoremap <c-l> <c-w>l
    nnoremap <c-h> <c-w>h
" --- Buffers (b)

" --- General vim shortcuts (v)
    " Edit config files
    nnoremap <leader>vei :split ~/.config/nvim/init.vim<CR>
    nnoremap <leader>vem :split ~/.config/nvim/mapping.vim<CR>
    nnoremap <leader>vep :split ~/.config/nvim/plugs.vim<CR>
    nnoremap <leader>vec :split ~/.config/nvim/plugconf.vim<CR>
    nnoremap <leader>veg :split ~/.config/nvim/general.vim<CR>
    " Reload with new config
    nnoremap <leader>vr :source ~/.config/nvim/init.vim<CR>

    " Set spell 
    nnoremap <silent><leader>vo :set spell!<CR>
    " Correct spelling in insert mode
    inoremap <c-l> <c-g>u<Esc>[s1z=`]a<c-g>u

    " Clear Search --- see vim docs
    nnoremap <silent><leader>vsc :let @/=""<CR>

" --- Move  vertically by visual line
    noremap <silent><expr> j (v:count ==0 ? 'gj' : 'j')
    noremap <silent><expr> k (v:count ==0 ? 'gk' : 'k')

" --- Fix holding shift for too long
    cnoremap W w
    cnoremap Q q
    cnoremap Qa qa

" --- Be quicker
    nnoremap <nowait><leader>w :w<CR>
    nnoremap <nowait><leader>W :wq<CR>
    nnoremap <nowait><leader>q :q<CR>

" --- Other things
    " Make it nicer to get to end
    nnoremap G G$

