"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ----------------------- Vim-plug for managing plugins -----------------------
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.local/share/nvim/site/plugged')
" --- Interface and eye-candy ---
    Plug 'itchyny/lightline.vim'		" Light and configurable status bar
    Plug 'morhetz/gruvbox' 			" My favourite colourscheme
    Plug 'scrooloose/nerdtree'			" File explorer
    Plug 'Xuyuanp/nerdtree-git-plugin'		" Add git symbols to NerdTree
    Plug 'ryanoasis/vim-devicons' 		" Cool icons in NerdTree etc.
    Plug 'airblade/vim-gitgutter'		" Show git chnages in signcolumn
    Plug 'junegunn/goyo.vim' 			" 'Focus mode' for writing
    "Plug 'tiagogfumo/vim-nerdtree-syntax-highlight"
    "Plug 'junegunn/limelight.vim'		" Dim surrounding paragraphs
" --- Other helpful plugins ---
    Plug 'jiangmiao/auto-pairs'
    "Plug 'vimwiki/vimwiki'
    Plug 'vifm/vifm.vim'			" Vifm in vim
" --- Coding help ---
    Plug 'tpope/vim-commentary'			" Easily comment out blocks of code
    Plug 'SirVer/ultisnips'			" Easy, fast snippets
    Plug 'tpope/vim-surround'			" Chnage surrounding characters easy
    Plug 'neoclide/coc.nvim', {'branch':'release'}	" Conquerer of Completion
    Plug 'jackguo380/vim-lsp-cxx-highlight'
" --- LaTeX writing ---
    Plug 'KeitaNakamura/tex-conceal.vim'	" Mask LaTeX commands
    Plug 'lervag/vimtex'			" Advanced latex support
" --- Python Programming ---
    " Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}

"--- Markdown ---
    Plug 'iamcco/markdown-preview.nvim', {'do':'cd app & yarn install'}
" --- Tmux Integration ---
    "Plug 'christoomey/vim-tmux-navigator'
" --- C++ ---
    Plug 'bfrg/vim-cpp-modern'
" --- R ---
    Plug 'jalvesaq/Nvim-R', {'branch': 'stable'}
call plug#end()
