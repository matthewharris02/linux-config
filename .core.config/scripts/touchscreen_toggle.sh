#! /bin/sh
state=$(xinput list-props "ELAN2514:00 04F3:29CF"  | grep "Device Enabled" | sed -n 's/.*\([1-9]\+\)*\([0-1]\).*/\2/p')
if [ "$state" = "1" ]; then
    xinput disable "ELAN2514:00 04F3:29CF"
fi
if [ "$state" = "0" ]; then
    xinput enable "ELAN2514:00 04F3:29CF"
fi
