#+TITLE: Matthew's Configuration Files

* Table of Contents :toc:
- [[#introduction][Introduction]]
- [[#my-software][My Software]]
- [[#licence][Licence]]
- [[#temp][(TEMP)]]

* Introduction
This git repo contains my configuration files for the software that I use on linux (currently [[https://arcolinux.com/][ArcoLinux]]).

* My Software
- Starship prompt
- Doom Emacs
- Neovim
- Qtile window manager

* Licence
All configurations in this repository are licenced under their parent software's licence, unless otherwise stated.

* (TEMP)
Fonts:
- From pacman:
  - ttf-tinos-nerd
  - ttf-noto-nerd
