#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PATH="/usr/lib/rstudio/:$HOME/.local/bin/${PATH:+:${PATH}}" # Add .local/bin/ to $PATH
export PATH="$HOME/.config/emacs/bin:$PATH"
export PATH="$HOME/.bin:$PATH"
export LD_LIBRARY_PATH=""
# Use EmacsClient as editor
export ALTERNATE_EDITOR=""
# export EDITOR="emacsclient -t"        #  editor opens in terminal
export VISUAL="emacsclient -c -a emacs" # $VISUAL opens in GUI mode

# Use NeoVim as editor
export EDITOR="nvim"
export TERM="xterm" # To fix not working with ssh
PS1='[\u@\h \W]\$ '

# Bash powerline
# powerline-daemon -q
# POWERLINE_BASH_CONTINUATION=1
# POWERLINE_BASH_SELECT=1
# . /usr/share/powerline/bindings/bash/powerline.sh

# Bash completion in kitty
#source <(kitty + complete setup bash)


# Set dircolors to my own version, which fixes highlighting on NTFS
eval "$(dircolors ~/.mydircolors)"
# ==== Aliases ====

# Some Useful Aliases
alias sp="sudo pacman"
alias ka="killall"
alias r="source ~/.bashrc"
alias er="killall emacs && emacs --daemon"
# navigation
alias ..='cd ..'
alias ...='cd ../..'

# vim and emacs
alias n='nvim'
alias sn="sudo nvim"
alias emacs='emacs'
# add flags
alias cp='cp -i'                #  Confirm before overwriting

alias ds='doom sync && killall emacs && emacs --daemon' # Sync and restart doom emacs
# Colour
alias ls="ls -hN --color=auto --group-directories-first"

# Starship
export STARSHIP_CONFIG=~/.config/starship.toml
eval "$(starship init bash)"
# NeoFetch
neofetch

# Config git control 
alias config='/usr/bin/git --git-dir=/home/matthew/.cfg/ --work-tree=/home/matthew'

# Dual monitor
alias extendr="xrandr --output HDMI-A-0 --mode 1920x1080 --right-of eDP & ~/.fehbg"
alias extendl="xrandr --output HDMI-A-0 --mode 1920x1080 --left-of eDP & ~/.fehbg"
alias single="xrandr --output HDMI-A-0 --off"

# Screen
alias soff="xset dpms force off"

# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
